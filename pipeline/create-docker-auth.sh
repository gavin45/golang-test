#! /busybox/sh
gitlabPipelineRegistry="$1"
gitlabPipelineUser="$2"
gitlabPipelinePass="$3"

cat << JSON
{
  "auths": {
    "${gitlabPipelineRegistry}": {
      "username":"${gitlabPipelineUser}",
      "password":"${gitlabPipelinePass}"
    }
  }
}
JSON